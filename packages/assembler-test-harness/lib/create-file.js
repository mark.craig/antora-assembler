'use strict'

const File = require('vinyl')

// FIXME this needs to be much more comprehensive
function createFile (src) {
  const familySegment = src.family + 's'
  const outPath = [
    src.component === 'ROOT' ? '' : src.component,
    src.version,
    src.module === 'ROOT' ? '' : src.module,
    src.family === 'page' ? '' : '_' + familySegment,
    src.family === 'page' ? src.relative.replace('.adoc', '.html') : src.relative,
  ]
    .filter((it) => it)
    .join('/')
  return new File({
    path: `modules/${src.module}/${familySegment}/${src.relative}`,
    contents: src.contents,
    src,
    out: { path: outPath },
    pub: {
      url: '/' + outPath,
      moduleRootPath: '',
    },
  })
}

module.exports = createFile
